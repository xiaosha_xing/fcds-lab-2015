# Foundations of Concurrent and Distributed Systems Lab: Summer semester 2015 #

This repository contains 5 programming tasks, with their descriptions, sequential C sources, and test inputs. The tasks are taken from the 7th Marathon of Parallel Programming WSCAD-SSC/SBAC-PAD-2012.

# Contributors #

Dmitrii Kuvaiskii <dmitrii.kuvaiskii@tu-dresden.de>

# List of Participants

* Miquel Palacios -- Java

* Cristof Leonhardt -- Java (Fork/Join)

* Maya Shallouf -- Erlang

* Peter Heisig -- Java

* Martin Rataj -- C++ & Rust

* Rouchun Tzeng -- C

* Jörg Thalheim -- Go

* Michael Ullrich -- C

* Javid Abbasov -- Java

* Florian Blume -- C++

* Paul Winter -- C++

* Leonardo Marques -- C

* Xing Xiaosha -- Java

* Shen Tong -- Java

* Johannes Karl -- C

* Antonio Monteiro -- Go

* Antonio Coelho -- Python

* Donghao Lu -- Java